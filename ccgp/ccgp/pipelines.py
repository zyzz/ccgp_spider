# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
from copy import deepcopy

from celery_app.tasks import insert


class SerializePipeline(object):

    def process_item(self, item, spider):
        insert.delay(**item)
        return item
