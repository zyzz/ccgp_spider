# -*- coding: utf-8 -*-

from scrapy import cmdline
from datetime import datetime

if __name__ == '__main__':
    cmdline.execute(
        "scrapy crawl ccgp -s CRAWL_DATE={0}".format(
            datetime.now().strftime('%Y-%m-%d')).split())
