import logging
from urllib.parse import urljoin
from datetime import datetime

import requests
from scrapy import cmdline
from requests.auth import HTTPBasicAuth

from .app import app

auth = HTTPBasicAuth(app.conf['HTTP_USER'], app.conf['HTTP_PASSWORD'])


@app.task
def insert(**kwargs):
    url = app.conf['HTTP_URL']
    data = {
        "title": kwargs['title'],
        "url": kwargs['url'],
        "date": kwargs['date'],
        "site": kwargs['site']
    }
    response = requests.post(urljoin(url, 'ccgp'), data, auth=auth)
    if response.status_code != 201:
        logging.error('{} insert error'.format(data['title']))


@app.task
def crawl():
    cmdline.execute(
        "scrapy crawl ccgp -s CRAWL_DATE={0}".format(
            datetime.now().strftime('%Y-%m-%d')).split())
