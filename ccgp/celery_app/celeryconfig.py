import os
from datetime import timedelta

BROKER_URL = os.environ.get('BROKER_URL', 'amqp://admin:admin@localhost:5672/')
CELERY_TASK_SERIALIZER = 'msgpack'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_ACCEPT_CONTENT = ['json', 'msgpack']

HTTP_URL = os.environ.get('HTTP_URL', 'http://localhost:9090')
HTTP_USER = os.environ.get('HTTP_USER', 'admin')
HTTP_PASSWORD = os.environ.get('HTTP_PASSWORD', 'admin')

CELERYBEAT_SCHEDULE = {
    'crawl': {
        'task': 'celery_app.tasks.crawl',
        'schedule': timedelta(minutes=10),
        'args': ()
    }
}
