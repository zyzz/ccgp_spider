import os

MONGO_HOST = os.environ.get('MONGO_HOST', 'localhost')
MONGO_PORT = int(os.environ.get('MONGO_PORT', 27017))
MONGO_DBNAME = os.environ.get('MONGO_DBNAME', 'ccgp')

X_HEADERS = ['Content-Type', 'If-Match']

RESOURCE_METHODS = ['GET', 'POST', 'DELETE']
ITEM_METHODS = ['GET', 'PATCH', 'DELETE']

PUBLIC_METHODS = ['GET']
PUBLIC_ITEM_METHODS = ['GET']

AUTH_FIELD = True

CACHE_CONTROL = 'max-age=20'
CACHE_EXPIRES = 20

ccgp = {
    'item_title': 'ccgp',

    'additional_lookup': {
        'url': 'regex("[\w]+")',
        'field': 'title'
    },

    'schema': {
        'title': {
            'type': 'string',
            'minlength': 2,
            'required': True,
            'unique': True,
        },
        'site': {
            'type': 'string',
        },
        'url': {
            'type': 'string',
            'maxlength': 260,
        },
        'date': {
            'type': 'string',
        },
    }
}

DOMAIN = {
    'ccgp': ccgp,
}
